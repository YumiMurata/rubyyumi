module Docente
    class Professor
        def initialize(nome,idade,salario,matricula,Departamento)
            @nome=nome
            @idade=idade
            @salario=salario
            @matricula=matricula
            @Departamento= departamento 
        end
        def hash{nome:@nome,idade:@idade,salario:@salario,matricula:@matricula,departamento:@Departamento}
        end
    class Departamento 
        def initialize( materias, predio, sigla)
            @materias =materias
            @predio =predio
            @sigla =sigla
        end
        def hash{materias:@materias,predio:@predios,sigla:@sigla}
        end
    end
end

module Discentes
    class Aluno
        def initialize(nome,matricula,periodo,curso)
            @nome=nome
            @matricula=matricula
            @periodo=periodo
            @curso=curso
        end
        def hash {nome:@nome,matricula:@matricula,periodo:@periodo,curso:@curso}
        end

    end
    class AlunoGraduacao < Aluno
        def initialize(cr, cargahoraria)
            @cr=cr
            @cargahoraria=cargahoraria
        end
        def hash{cr:@cr,cargahoraria:@cargahoraria}
        end
    end
    class AlunoMestrado < Aluno
        def initialize(graduacao, orientador, areapesquisa)
            @graducao=graducao
            @orientador=orientador
            @areapesquisa=areapesquisa
        end
        def hash{graduacao:@graducao,orientador:@orientador,areapesquisa:@areapesquisa}
        end
end

module Bd
    class Dados
        def initialize
         @array = []
        end
        def salvar (obj)
            @array.push(obj)
        end
        def printar(@array)
        puts @array
        end
    end
end

